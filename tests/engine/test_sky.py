import pytest

from boyds import Sky, Bird, Coordinate
from boyds.engine.bird import Horizon, Direction
from boyds.engine.sky import Quadrant, CrowdedSky, is_in_blind_spot, distance


class SkyPropertiesTest:
    @pytest.mark.parametrize(
        'sky,is_root',
        [
            (Sky(None, 2, 2, None), True),
            (Sky(Sky(None, 1, 1, None), 2, 2, None), False)
        ]
    )
    def test_is_root(self, sky, is_root):
        assert sky.is_root == is_root

    def test_is_leaf_is_true_if_no_children(self):
        root = Sky(None, 1, 1, None)
        sky = Sky(root, 2, 2, None)
        assert sky.is_leaf is True

    def test_is_leaf_is_false_if_children(self):
        sky = Sky(None, 2, 2, None)
        sky.insert(Bird())
        assert sky.is_leaf is False

    def test_center_is_computed_from_dimensions_if_root(self):
        sky = Sky(None, 2, 2, None)
        assert sky.center == Coordinate(1, 1)

    def test_center_is_computed_on_insert_for_children_in_north_east(self):
        sky = Sky(None, 4, 4, None)
        sky.insert(Bird(Coordinate(2, 2)))
        assert sky.north_east.center == Coordinate(3, 3)

    def test_center_is_computed_on_insert_for_children_in_north_west(self):
        sky = Sky(None, 4, 4, None)
        sky.insert(Bird(Coordinate(-2, 2)))
        assert sky.north_west.center == Coordinate(1, 3)

    def test_center_is_computed_on_insert_for_children_in_south_west(self):
        sky = Sky(None, 4, 4, None)
        sky.insert(Bird(Coordinate(-2, -2)))
        assert sky.south_west.center == Coordinate(1, 1)

    def test_center_is_computed_on_insert_for_children_in_south_east(self):
        sky = Sky(None, 4, 4, None)
        sky.insert(Bird(Coordinate(2, -2)))
        assert sky.south_east.center == Coordinate(3, 1)

    def test_returns_a_list_of_boids(self):
        sky = Sky(None, 10, 10, None)
        bird_1 = Bird(Coordinate(1, 1))
        bird_2 = Bird(Coordinate(1, 2))
        bird_3 = Bird(Coordinate(-1, 3))
        bird_4 = Bird(Coordinate(-2, 5))
        bird_5 = Bird(Coordinate(-4, -4))
        bird_6 = Bird(Coordinate(3, 5))
        bird_7 = Bird(Coordinate(1, -2))
        sky.insert(bird_1)
        sky.insert(bird_2)
        sky.insert(bird_3)
        sky.insert(bird_4)
        sky.insert(bird_5)
        sky.insert(bird_6)
        sky.insert(bird_7)

        boids = sky.boids

        assert len(boids) == 7
        assert bird_1 in boids
        assert bird_2 in boids
        assert bird_3 in boids
        assert bird_4 in boids
        assert bird_5 in boids
        assert bird_6 in boids
        assert bird_7 in boids


class InsertTest:
    @pytest.mark.parametrize(
        'coordinate,quadrant',
        [
            (Coordinate(1, 3), Quadrant.NORTH_WEST),
            (Coordinate(3, 3), Quadrant.NORTH_EAST),
            (Coordinate(3, 1), Quadrant.SOUTH_EAST),
            (Coordinate(1, 1), Quadrant.SOUTH_WEST),
        ]
    )
    def test_insert_creates_a_sub_sky_if_needed_and_puts_the_boid_in(self, coordinate, quadrant):
        sky = Sky(None, 4, 4, None)
        assert not sky.children
        boid = Bird(coordinate)

        sky.insert(boid)

        assert sky.children[0].quadrant == quadrant

    def test_insert_creates_a_deeper_sub_sky_if_needed_if_boid_already_in_quadrant(self):
        sky = Sky(None, 10, 10, None)
        bird_1 = Bird(Coordinate(3, 3))
        bird_2 = Bird(Coordinate(3, 4))
        sky.insert(bird_1)

        sky.insert(bird_2)

        assert sky.children[0].boid == bird_1
        assert sky.children[0].children[0].boid == bird_2
        assert sky.children[0].children[0].quadrant == Quadrant.NORTH_EAST

    def test_raises_an_exception_if_a_bird_already_is_there(self):
        sky = Sky(None, 10, 10, None)
        bird_1 = Bird(Coordinate(3, 3))
        bird_2 = Bird(Coordinate(3, 4))
        bird_3 = Bird(Coordinate(3, 4))
        sky.insert(bird_1)
        sky.insert(bird_2)

        with pytest.raises(CrowdedSky):
            sky.insert(bird_3)

        assert sky.children[0].boid == bird_1
        assert sky.children[0].children[0].boid == bird_2


class FindClosestNeighboursTest:
    def test_returns_a_list_of_boids(self):
        sky = Sky(None, 30, 30, None)
        looker_boid = Bird(Coordinate(23, 23), horizon=Horizon(8))
        sky.insert(looker_boid)
        sky.insert(Bird(Coordinate(28, 28)))
        sky.insert(Bird(Coordinate(18, 18)))
        sky.insert(Bird(Coordinate(10, 10)))
        sky.insert(Bird(Coordinate(23, 2)))
        sky.insert(Bird(Coordinate(2, 23)))

        neighbours = sky.find_closest_neighbours(looker_boid)

        assert len(neighbours) == 2


class IsInBlindSpotTest:
    @pytest.mark.parametrize(
        'boid',
        [
            Bird(Coordinate(0, 1), Direction(0)),
            Bird(Coordinate(1, 1), Direction(45)),
            Bird(Coordinate(1, 0), Direction(90)),
            Bird(Coordinate(1, -1), Direction(135)),
            Bird(Coordinate(0, -1), Direction(180)),
            Bird(Coordinate(-1, -1), Direction(225)),
            Bird(Coordinate(-1, 0), Direction(270)),
            Bird(Coordinate(-1, 1), Direction(315))
        ]
    )
    def test_with_neighbour_on_origin_and_boid_looking_forward(self, boid):
        assert is_in_blind_spot(boid, Bird()) is True

    def test_with_neighbour_on_the_right(self):
        boid = Bird(Coordinate(0, 1))
        neighbour = Bird(Coordinate(1, 1))
        assert is_in_blind_spot(boid, neighbour) is False

    def test_with_neighbour_on_the_left(self):
        boid = Bird(Coordinate(0, 1))
        neighbour = Bird(Coordinate(-1, 1))
        assert is_in_blind_spot(boid, neighbour) is False

    def test_with_neighbour_in_front(self):
        boid = Bird(Coordinate(0, 1))
        neighbour = Bird(Coordinate(0, 2))
        assert is_in_blind_spot(boid, neighbour) is False

    @pytest.mark.parametrize(
        'boid',
        [
            Bird(Coordinate(0, 1), Direction(180)),
            Bird(Coordinate(1, 1), Direction(225)),
            Bird(Coordinate(1, 0), Direction(270)),
            Bird(Coordinate(1, -1), Direction(315)),
            Bird(Coordinate(0, -1), Direction(0)),
            Bird(Coordinate(-1, -1), Direction(45)),
            Bird(Coordinate(-1, 0), Direction(90)),
            Bird(Coordinate(-1, 1), Direction(135))
        ]
    )
    def test_with_neighbour_on_origin_and_boid_looking_backward(self, boid):
        assert is_in_blind_spot(boid, Bird()) is False

    @pytest.mark.parametrize(
        'boid,neighbour',
        [
            (Bird(Coordinate(0, 1), Direction(0)), Bird(Coordinate(0, 2))),
            (Bird(Coordinate(1, 1), Direction(45)), Bird(Coordinate(2, 2))),
            (Bird(Coordinate(1, 0), Direction(90)), Bird(Coordinate(2, 0))),
            (Bird(Coordinate(1, -1), Direction(135)), Bird(Coordinate(2, -2))),
            (Bird(Coordinate(0, -1), Direction(180)), Bird(Coordinate(0, -2))),
            (Bird(Coordinate(-1, -1), Direction(225)), Bird(Coordinate(-2, -2))),
            (Bird(Coordinate(-1, 0), Direction(270)), Bird(Coordinate(-2, 0))),
            (Bird(Coordinate(-1, 1), Direction(315)), Bird(Coordinate(-2, 2)))
        ]
    )
    def test_with_neighbour_in_front_and_boid_looking_forward(self, boid, neighbour):
        assert is_in_blind_spot(boid, neighbour) is False

    @pytest.mark.parametrize(
        'boid,neighbour',
        [
            (Bird(Coordinate(0, 1), Direction(180)), Bird(Coordinate(0, 2))),
            (Bird(Coordinate(1, 1), Direction(225)), Bird(Coordinate(2, 2))),
            (Bird(Coordinate(1, 0), Direction(270)), Bird(Coordinate(2, 0))),
            (Bird(Coordinate(1, -1), Direction(315)), Bird(Coordinate(2, -2))),
            (Bird(Coordinate(0, -1), Direction(0)), Bird(Coordinate(0, -2))),
            (Bird(Coordinate(-1, -1), Direction(45)), Bird(Coordinate(-2, -2))),
            (Bird(Coordinate(-1, 0), Direction(90)), Bird(Coordinate(-2, 0))),
            (Bird(Coordinate(-1, 1), Direction(135)), Bird(Coordinate(-2, 2)))
        ]
    )
    def test_with_neighbour_in_front_and_boid_looking_backward(self, boid, neighbour):
        assert is_in_blind_spot(boid, neighbour) is True


class DistanceTest:
    def test_no_distance_at_origin(self):
        a = Bird(Coordinate(x=0, y=0))
        b = Bird(Coordinate(x=0, y=0))

        assert distance(a, b) == 0

    def test_on_x_axis(self):
        a = Bird(Coordinate(x=0, y=0))
        b = Bird(Coordinate(x=3, y=0))

        assert distance(a, b) == 3

    def test_on_y_axis(self):
        a = Bird(Coordinate(x=0, y=0))
        b = Bird(Coordinate(x=0, y=3))

        assert distance(a, b) == 3

    @pytest.mark.parametrize(
        'a,b,result',
        [
            (Coordinate(x=0, y=0), Coordinate(x=2, y=2), 2.83),
            (Coordinate(x=-3, y=7), Coordinate(x=2, y=-5), 13),
            (Coordinate(x=-120, y=-237), Coordinate(x=58, y=71), 355.74)
        ]
    )
    def test_between_ordinary_points(self, a, b, result):
        assert distance(Bird(a), Bird(b)) == result
