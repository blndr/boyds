import pytest

from boyds.engine.bird import Reaction, Neighbour, Direction, Coordinate, Horizon, Bird, Speed, Edge, Color
from boyds.engine.constants import MIN_SPEED, MAX_SPEED, STEERING_THRESHOLD, CLOSING_THRESHOLD, ACCELERATION_INCREMENT, \
    HORIZON_INCREMENT
from boyds.engine.reaction import adapt_speed, adapt_horizon, chose_reaction, move, detect_edge, adapt_color


class AdaptSpeedTest:
    def test_increase_speed_in_repulsion(self):
        boid = Bird(speed=Speed(MAX_SPEED - 1))
        speed = adapt_speed(boid, Reaction.REPULSION)
        assert speed == Speed(MAX_SPEED)

    def test_decrease_speed_in_orientation(self):
        boid = Bird(speed=Speed(MAX_SPEED))
        speed = adapt_speed(boid, Reaction.ORIENTATION)
        assert speed == Speed(MAX_SPEED - ACCELERATION_INCREMENT)

    def test_increase_speed_in_attraction(self):
        boid = Bird(speed=Speed(MIN_SPEED))
        speed = adapt_speed(boid, Reaction.ATTRACTION)
        assert speed == Speed(MIN_SPEED + ACCELERATION_INCREMENT)

    def test_decrease_speed_in_steady(self):
        boid = Bird(speed=Speed(MAX_SPEED))
        speed = adapt_speed(boid, Reaction.STEADY)
        assert speed == Speed(MAX_SPEED - ACCELERATION_INCREMENT)

    def test_minimum_speed(self):
        boid = Bird(speed=Speed(MIN_SPEED))
        speed = adapt_speed(boid, Reaction.ORIENTATION)
        assert speed == Speed(MIN_SPEED)

    def test_maximum_speed(self):
        boid = Bird(speed=Speed(MAX_SPEED))
        speed = adapt_speed(boid, Reaction.ATTRACTION)
        assert speed == Speed(MAX_SPEED)


class AdaptHorizonTest:
    @pytest.mark.parametrize(
        'reaction,delta',
        [
            (Reaction.STEADY, HORIZON_INCREMENT),
            (Reaction.ORIENTATION, -HORIZON_INCREMENT),
            (Reaction.ATTRACTION, HORIZON_INCREMENT),
            (Reaction.REPULSION, -HORIZON_INCREMENT),
        ]
    )
    def test_horizon_adaptation_by_reaction(self, reaction, delta):
        boid = Bird(horizon=Horizon(60))
        horizon = adapt_horizon(boid, reaction)
        assert horizon.meters == 60 + delta


class ChoseReactionTest:
    class WithAnEdgeTest:
        @pytest.mark.parametrize(
            'direction,reaction',
            [
                (Direction(angle=0), Reaction.RIGHT_TURN),
                (Direction(angle=315), Reaction.RIGHT_TURN),
                (Direction(angle=270), Reaction.LEFT_TURN),
                (Direction(angle=225), Reaction.LEFT_TURN),
                (Direction(angle=180), Reaction.LEFT_TURN)
            ]
        )
        def test_reaction_on_a_left_edge(self, direction, reaction):
            assert chose_reaction(Bird(direction=direction), [], [Edge.LEFT]) == reaction

        @pytest.mark.parametrize(
            'direction,reaction',
            [
                (Direction(angle=0), Reaction.LEFT_TURN),
                (Direction(angle=45), Reaction.LEFT_TURN),
                (Direction(angle=90), Reaction.LEFT_TURN),
                (Direction(angle=135), Reaction.RIGHT_TURN),
                (Direction(angle=180), Reaction.RIGHT_TURN)
            ]
        )
        def test_reaction_on_a_right_edge(self, direction, reaction):
            assert chose_reaction(Bird(direction=direction), [], [Edge.RIGHT]) == reaction

        @pytest.mark.parametrize(
            'direction,reaction',
            [
                (Direction(angle=90), Reaction.LEFT_TURN),
                (Direction(angle=135), Reaction.LEFT_TURN),
                (Direction(angle=180), Reaction.RIGHT_TURN),
                (Direction(angle=225), Reaction.RIGHT_TURN),
                (Direction(angle=270), Reaction.RIGHT_TURN)
            ]
        )
        def test_reaction_on_a_bottom_edge(self, direction, reaction):
            assert chose_reaction(Bird(direction=direction), [], [Edge.BOTTOM]) == reaction

        @pytest.mark.parametrize(
            'direction,reaction',
            [
                (Direction(angle=270), Reaction.LEFT_TURN),
                (Direction(angle=315), Reaction.LEFT_TURN),
                (Direction(angle=359), Reaction.LEFT_TURN),
                (Direction(angle=0), Reaction.RIGHT_TURN),
                (Direction(angle=1), Reaction.RIGHT_TURN),
                (Direction(angle=45), Reaction.RIGHT_TURN),
                (Direction(angle=90), Reaction.RIGHT_TURN)
            ]
        )
        def test_reaction_on_a_top_edge(self, direction, reaction):
            assert chose_reaction(Bird(direction=direction), [], [Edge.TOP]) == reaction

    class WithNoEdgeTest:
        @pytest.mark.parametrize(
            'neighbours,reaction',
            [
                (
                        [Neighbour(Bird(), STEERING_THRESHOLD + 1), Neighbour(Bird(), STEERING_THRESHOLD - 1)],
                        Reaction.REPULSION
                ),
                (
                        [Neighbour(Bird(), STEERING_THRESHOLD + 2), Neighbour(Bird(), STEERING_THRESHOLD - 2)],
                        Reaction.REPULSION
                )
            ]
        )
        def test_reaction_when_birds_are_too_close_is_repulsion(self, neighbours, reaction):
            assert chose_reaction(Bird(), neighbours, []) == reaction

        @pytest.mark.parametrize(
            'neighbours,reaction',
            [
                (
                        [Neighbour(Bird(), CLOSING_THRESHOLD + 1), Neighbour(Bird(), CLOSING_THRESHOLD - 1)],
                        Reaction.ORIENTATION
                ),
                (
                        [Neighbour(Bird(), CLOSING_THRESHOLD + 2), Neighbour(Bird(), CLOSING_THRESHOLD - 2)],
                        Reaction.ORIENTATION
                ),
            ]
        )
        def test_reaction_when_birds_are_reasonably_close_is_orientation(self, neighbours, reaction):
            assert chose_reaction(Bird(), neighbours, []) == reaction

        @pytest.mark.parametrize(
            'neighbours,reaction',
            [
                ([Neighbour(Bird(), CLOSING_THRESHOLD + 1)], Reaction.ATTRACTION),
                ([Neighbour(Bird(), CLOSING_THRESHOLD + 2)], Reaction.ATTRACTION),
                ([Neighbour(Bird(), CLOSING_THRESHOLD + 3)], Reaction.ATTRACTION)
            ]
        )
        def test_reaction_when_birds_are_far_is_attraction(self, neighbours, reaction):
            assert chose_reaction(Bird(horizon=Horizon(50)), neighbours, []) == reaction

        @pytest.mark.parametrize(
            'neighbours,reaction',
            [
                ([Neighbour(Bird(), 51)], Reaction.STEADY),
                ([Neighbour(Bird(), 52)], Reaction.STEADY),
                ([Neighbour(Bird(), 53)], Reaction.STEADY)
            ]
        )
        def test_reaction_when_birds_are_beyond_horizon_is_steady(self, neighbours, reaction):
            assert chose_reaction(Bird(horizon=Horizon(50)), neighbours, []) == reaction


class DetectEdgeTest:
    def test_left_edge_detection(self):
        boid = Bird(Coordinate(30, 200))
        assert detect_edge(boid, 0, 1000, 0, 1000) == [Edge.LEFT]

    def test_right_edge_detection(self):
        boid = Bird(Coordinate(970, 200))
        assert detect_edge(boid, 0, 1000, 0, 1000) == [Edge.RIGHT]

    def test_bottom_edge_detection(self):
        boid = Bird(Coordinate(200, 30))
        assert detect_edge(boid, 0, 1000, 0, 1000) == [Edge.BOTTOM]

    def test_top_edge_detection(self):
        boid = Bird(Coordinate(200, 970))
        assert detect_edge(boid, 0, 1000, 0, 1000) == [Edge.TOP]

    def test_no_edge_to_detect(self):
        boid = Bird(Coordinate(200, 200))
        assert detect_edge(boid, 0, 1000, 0, 1000) == []


class AdaptColorTest:
    @pytest.mark.parametrize(
        'reaction,color',
        [
            (Reaction.STEADY, Color.WHITE),
            (Reaction.REPULSION, Color.RED),
            (Reaction.ATTRACTION, Color.ORANGE),
            (Reaction.ORIENTATION, Color.GREEN),
            (Reaction.LEFT_TURN, Color.PINK),
            (Reaction.RIGHT_TURN, Color.PINK)
        ]
    )
    def test_matches_a_color_with_a_reaction(self, reaction, color):
        assert adapt_color(reaction) == color


class MoveTest:
    @pytest.mark.parametrize(
        'direction,coordinate',
        [
            (Direction(0), Coordinate(0, 5)),
            (Direction(90), Coordinate(5, 0)),
            (Direction(180), Coordinate(0, -5)),
            (Direction(270), Coordinate(-5, 0))
        ]
    )
    def test_moving_on_axis_from_origin(self, direction, coordinate):
        result = move(Bird(Coordinate(0, 0), speed=Speed(5)), direction)
        assert result == coordinate

    @pytest.mark.parametrize(
        'direction,coordinate',
        [
            (Direction(45), Coordinate(3.54, 3.54)),
            (Direction(135), Coordinate(3.54, -3.54)),
            (Direction(225), Coordinate(-3.54, -3.54)),
            (Direction(315), Coordinate(-3.54, 3.54))
        ]
    )
    def test_moving_on_diagonal_from_origin(self, direction, coordinate):
        result = move(Bird(Coordinate(0, 0), speed=Speed(5)), direction)
        assert result == coordinate

    @pytest.mark.parametrize(
        'boid,coordinate',
        [
            (Bird(Coordinate(2, 1), Direction(30), speed=Speed(5)), Coordinate(4.5, 5.33)),
            (Bird(Coordinate(3, 4), Direction(225), speed=Speed(5)), Coordinate(-0.54, 0.46)),
            (Bird(Coordinate(-7, -12), Direction(45), speed=Speed(5)), Coordinate(-3.46, -8.46)),
            (Bird(Coordinate(-67, 90), Direction(108), speed=Speed(5)), Coordinate(-62.24, 88.45))
        ]
    )
    def test_moving_on_diagonal_from_any_point(self, boid, coordinate):
        result = move(boid, boid.direction)
        assert result == coordinate
