from unittest.mock import patch

import pytest

from boyds.engine.bird import Reaction, Neighbour, Bird, Coordinate, Direction
from boyds.engine.constants import STEERING_AMPLITUDE, AVOIDANCE_AMPLITUDE
from boyds.engine.steering import steer


class SteerTest:
    class SteerToTurnTest:
        @pytest.mark.parametrize('angle', [123, 3, 90])
        def test_direction_changes_by_an_avoidance_amplitude_on_the_left(self, angle):
            boid = Bird(direction=Direction(angle))
            assert steer(boid, Reaction.LEFT_TURN, []) == Direction(angle - AVOIDANCE_AMPLITUDE)

        @pytest.mark.parametrize('angle', [123, 3, 356])
        def test_direction_changes_by_an_avoidance_amplitude_on_the_right(self, angle):
            boid = Bird(direction=Direction(angle))
            assert steer(boid, Reaction.RIGHT_TURN, []) == Direction(angle + AVOIDANCE_AMPLITUDE)

    class SteerToAlignmentTest:
        def test_direction_does_not_change_with_no_neighbours(self):
            boid = Bird(direction=Direction(123))
            assert steer(boid, Reaction.ORIENTATION, []) == Direction(123)

        def test_direction_does_not_change_if_neighbours_are_aligned(self):
            boid = Bird(direction=Direction(30))
            neighbours = [
                Neighbour(Bird(Coordinate(0, 1), direction=Direction(15)), distance=1),
                Neighbour(Bird(Coordinate(1, 1), direction=Direction(45)), distance=1)
            ]
            assert steer(boid, Reaction.ORIENTATION, neighbours) == Direction(30)

        def test_steers_toward_the_average_direction_clockwise(self):
            boid = Bird(Coordinate(0, 0), direction=Direction(356))
            neighbours = [
                Neighbour(Bird(Coordinate(0, 1), direction=Direction(15)), distance=1),
                Neighbour(Bird(Coordinate(1, 1), direction=Direction(30)), distance=1)
            ]
            result = steer(boid, Reaction.ORIENTATION, neighbours)
            assert result - boid.direction == Direction(STEERING_AMPLITUDE)

        def test_steers_toward_the_average_direction_counter_clockwise(self):
            boid = Bird(Coordinate(1, 0))
            neighbours = [
                Neighbour(Bird(Coordinate(0, 1), direction=Direction(345)), distance=1),
                Neighbour(Bird(Coordinate(-1, 1), direction=Direction(330)), distance=1)
            ]
            result = steer(boid, Reaction.ORIENTATION, neighbours)
            assert result - boid.direction == Direction(360 - STEERING_AMPLITUDE)

    class SteerToCohesionTest:
        def test_direction_does_not_change_with_no_neighbours(self):
            boid = Bird(Coordinate(-1, -1), Direction(123))
            assert steer(boid, Reaction.ATTRACTION, []) == Direction(123)

        def test_direction_does_not_change_if_center_of_mass_of_neighbours_on_same_x_as_boid(self):
            boid = Bird(direction=Direction(30))
            neighbours = [
                Neighbour(Bird(Coordinate(-1, 1), direction=Direction(15)), distance=1),
                Neighbour(Bird(Coordinate(1, 1), direction=Direction(45)), distance=1)
            ]
            assert steer(boid, Reaction.ATTRACTION, neighbours) == Direction(30)

        def test_direction_does_not_change_if_center_of_mass_of_neighbours_on_same_y_as_boid(self):
            boid = Bird(direction=Direction(30))
            neighbours = [
                Neighbour(Bird(Coordinate(1, -1), direction=Direction(15)), distance=1),
                Neighbour(Bird(Coordinate(1, 1), direction=Direction(45)), distance=1)
            ]
            assert steer(boid, Reaction.ATTRACTION, neighbours) == Direction(30)

        def test_direction_does_not_change_if_already_toward_center_of_mass_of_neighbours(self):
            boid = Bird(direction=Direction(45))
            neighbours = [
                Neighbour(Bird(Coordinate(1, 1), direction=Direction(15)), distance=1),
                Neighbour(Bird(Coordinate(2, 2), direction=Direction(45)), distance=1)
            ]
            assert steer(boid, Reaction.ATTRACTION, neighbours) == Direction(45)

        def test_steers_toward_the_center_of_mass_of_neighbours_clockwise(self):
            boid = Bird(Coordinate(-1, -1), Direction(356))
            neighbours = [
                Neighbour(Bird(Coordinate(1, 1)), 5),
                Neighbour(Bird(Coordinate(2, 1)), 5)
            ]
            result = steer(boid, Reaction.ATTRACTION, neighbours)
            assert result == Direction((boid.direction.angle + 360 + STEERING_AMPLITUDE) % 360)

        def test_steers_toward_the_center_of_mass_of_neighbours_counter_clockwise(self):
            boid = Bird(Coordinate(1, 1), Direction(1))
            neighbours = [
                Neighbour(Bird(Coordinate(-1, 1)), 5),
                Neighbour(Bird(Coordinate(-2, 4)), 5)
            ]
            result = steer(boid, Reaction.ATTRACTION, neighbours)
            assert result == Direction(boid.direction.angle + 360 - STEERING_AMPLITUDE)

    class SteerToSeparationTest:
        def test_direction_does_not_change_with_no_neighbours(self):
            boid = Bird(Coordinate(-1, -1), Direction(123))
            assert steer(boid, Reaction.REPULSION, []) == Direction(123)

        @patch('boyds.engine.steering.random.choice', return_value=STEERING_AMPLITUDE)
        def test_steers_away_from_the_current_direction_clockwise(self, fake_choice):
            boid = Bird(Coordinate(-1, 1), direction=Direction(356))
            neighbours = [Neighbour(Bird(Coordinate(-4, 4)), 5)]
            result = steer(boid, Reaction.REPULSION, neighbours)
            assert result - boid.direction == Direction(STEERING_AMPLITUDE)
            assert result == Direction((boid.direction.angle + 360 + STEERING_AMPLITUDE) % 360)

        @patch('boyds.engine.steering.random.choice', return_value=-STEERING_AMPLITUDE)
        def test_steers_away_from_the_current_direction_counter_clockwise(self, fake_choice):
            boid = Bird(Coordinate(-1, 1), direction=Direction(1))
            neighbours = [Neighbour(Bird(Coordinate(-4, 4)), 5)]
            result = steer(boid, Reaction.REPULSION, neighbours)
            assert result == Direction(boid.direction.angle + 360 - STEERING_AMPLITUDE)
