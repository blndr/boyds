Boyds : Bird-oids in Python
===========================
Read => https://en.wikipedia.org/wiki/Boids

![Demo](screencast.mp4)

Setup
-----
```
virtualenv .venv -p python3.8
pip install -r requirements.txt
```

Running the simulation
----------------------
```
source .venv/bin/activate
python boids.py
```

### CLI options
- `fps` : refresh rate of the boids positions from 1 to 60. Defaults to 30. Example : `--fps=45`
- `duration` : duration in seconds of the simulation. Defaults to none. Leave blank for an infinite simulation. Example : `--duration=25`
- `size` : number of boids in the flock. Defaults to 50. Example : `--size=75`
- `fullscreen` : display mode of the window. Defaults to False. Example : `--fullscreen`

### Interacting with the simulation
By clicking and dragging the mouse on the window, you will generate new flocks.
Press `esc` or `alt+F4` to close the simulation.

Running tests
-------------
```
source .venv/bin/activate
pytest tests
```

System dependencies
-------------------
+ Python3.8
+ virtualenv
+ pip

Python dependencies
-------------------
See [requirements.txt](requirements.txt). Everything is available on Pypi.