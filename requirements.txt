pytest==5.4.2
pytest-cov==2.9.0
mypy==0.770
flake8==3.8.2
pyglet==1.5.5
ipdb==0.13.2