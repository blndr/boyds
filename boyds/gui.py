from math import degrees, atan2

import pyglet
from pyglet import clock
from pyglet.window import key, FPSDisplay

from boyds import generate, interact, Color
from boyds.generation import generate_single


class BoydsWindow(pyglet.window.Window):
    def __init__(self, cli_args, *args, **kwargs):
        if cli_args.fullscreen:
            super().__init__(fullscreen=cli_args.fullscreen, *args, **kwargs)
        else:
            super().__init__(width=960, height=720, *args, **kwargs)

        size = self.get_size()
        self.window_width, self.window_height = size[0], size[1]
        self.boids = generate(size=int(cli_args.size), max_x=self.window_width, max_y=self.window_height)
        self.is_paused = False
        self.triangle = pyglet.image.load('sprites/triangle.png')
        self.triangle.anchor_x = self.triangle.width // 2
        self.triangle.anchor_y = self.triangle.height // 2
        self.fps_display = BoydsFPSDisplay(window=self)
        self.fps = int(cli_args.fps)
        self.chrono = 0
        self.total_time = int(cli_args.duration) if cli_args.duration else None

        self.label_keys = pyglet.text.Label(
            '[esc] close | [c] clear | [space] (un)pause | click & drag to add',
            font_size=20,
            color=(145, 145, 145, 145),
            x=self.window_width / 2, y=self.window_height - 10,
            anchor_x='center', anchor_y='top'
        )

        self.label_paused = pyglet.text.Label(
            'PAUSE',
            font_size=54,
            color=(145, 145, 145, 145),
            x=self.window_width / 2, y=self.window_height / 2,
            anchor_x='center', anchor_y='center'
        )

    def on_draw(self, ):
        self.clear()
        self.label_boids = pyglet.text.Label(
            f"{len(self.boids)} boids",
            font_size=20,
            color=(145, 145, 145, 145),
            x=self.window_width - 10, y=10,
            anchor_x='right', anchor_y='bottom'
        )

        batch = pyglet.graphics.Batch()
        sprites = []
        for boid in self.boids:
            sprite = pyglet.sprite.Sprite(
                self.triangle,
                x=boid.coordinate.x,
                y=boid.coordinate.y,
                batch=batch,
            )
            sprite.scale = 0.15
            sprite.rotation = boid.direction.angle
            sprite.color = boid.color.value
            sprites.append(sprite)

        batch.draw()

        if self.is_paused:
            self.label_paused.draw()

        self.label_timer = pyglet.text.Label(
            f'{round(self.chrono, 1)} seconds',
            font_size=20,
            color=(145, 145, 145, 145),
            x=self.window_width / 2, y=10,
            anchor_x='center', anchor_y='bottom'
        )

        self.label_timer.draw()
        self.label_keys.draw()
        self.label_boids.draw()
        self.fps_display.draw()

    def on_close(self):
        exit(0)

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        steering_polar_degrees = int(degrees(atan2(dy, dx)))
        steering_absolute_degrees = ((-(steering_polar_degrees + 360) % 360) + 90) % 360
        self.boids.append(generate_single(x, y, steering_absolute_degrees, Color.WHITE))

    def on_key_press(self, symbol, modifiers):
        if symbol == key.C:
            self.boids = []

        elif symbol == key.ESCAPE:
            exit(0)

        elif symbol == key.SPACE:
            scheduled_functions = map(lambda item: item.func, clock.get_default()._schedule_interval_items)

            if self.move in scheduled_functions:
                clock.unschedule(self.move)
                self.is_paused = True
            else:
                clock.schedule_interval(self.move, 1 / self.fps)
                self.is_paused = False

    def move(self, dt):
        self.chrono += dt

        if self.total_time is not None:
            if self.chrono >= self.total_time:
                exit(0)

        moved_flock = interact(self.boids, self.width, self.height)
        self.boids = list(filter(
            lambda b: 0 < b.coordinate.x < self.window_width and 0 < b.coordinate.y < self.window_height,
            moved_flock
        ))


class BoydsFPSDisplay(FPSDisplay):
    def __init__(self, window):
        super().__init__(window)
        self.label = pyglet.text.Label(
            '',
            font_size=20,
            color=(145, 145, 145, 145),
            x=10, y=10,
            anchor_x='left', anchor_y='bottom'
        )

    def set_fps(self, fps):
        self.label.text = '%.2f' % fps + ' fps'
