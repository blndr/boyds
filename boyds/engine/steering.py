import random
from math import degrees, atan
from typing import List

from boyds.engine.bird import Neighbour, Reaction, Bird, Direction
from boyds.engine.constants import STEERING_AMPLITUDE, AVOIDANCE_AMPLITUDE


def steer(boid: Bird, reaction: Reaction, neighbours: List[Neighbour]) -> Direction:
    if reaction == reaction.LEFT_TURN:
        return Direction(boid.direction.angle - AVOIDANCE_AMPLITUDE)

    if reaction == reaction.RIGHT_TURN:
        return Direction(boid.direction.angle + AVOIDANCE_AMPLITUDE)

    if not neighbours:
        return boid.direction

    if reaction == Reaction.REPULSION:
        return Direction(boid.direction.angle + random.choice([-STEERING_AMPLITUDE, STEERING_AMPLITUDE]))

    if reaction == Reaction.ATTRACTION:
        new_steering = _steer_to_cohesion(boid, neighbours)
    else:
        new_steering = _steer_to_alignment(neighbours)

    boid_angle = _polarize_angle(boid.direction.angle)

    if new_steering > boid_angle:
        return Direction(boid.direction.angle + STEERING_AMPLITUDE)
    elif new_steering < boid_angle:
        return Direction(boid.direction.angle - STEERING_AMPLITUDE)
    else:
        return boid.direction


def _steer_to_alignment(neighbours: List[Neighbour]) -> int:
    angles = map(lambda n: n.boid.direction.angle, neighbours)
    polarized_angles = map(lambda a: -(360 - a) if a > 180 else a, angles)
    new_steering = int(sum(polarized_angles) // len(neighbours))
    return new_steering


def _steer_to_cohesion(boid: Bird, neighbours: List[Neighbour]) -> int:
    xs = map(lambda n: n.boid.coordinate.x, neighbours)
    ys = map(lambda n: n.boid.coordinate.y, neighbours)
    average_x, average_y = sum(xs) / len(neighbours), sum(ys) / len(neighbours)
    x, y = boid.coordinate.x, boid.coordinate.y
    delta_x, delta_y = (average_x - x), (average_y - y)

    if delta_y == 0 or delta_x == 0:
        return boid.direction.angle

    new_steering = degrees(atan(delta_y / delta_x))
    return int(new_steering)


def _polarize_angle(angle: int) -> int:
    return -(360 - angle) if angle > 180 else angle
