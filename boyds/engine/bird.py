from dataclasses import dataclass
from enum import Enum
from typing import Tuple

from boyds.engine.constants import MIN_SPEED, MIN_HORIZON


class Color(Enum):
    WHITE = (255, 255, 255)
    RED = (217, 54, 54)
    ORANGE = (217, 111, 54)
    YELLOW = (217, 214, 69)
    LIME = (150, 217, 69)
    GREEN = (86, 217, 69)
    BLUE_GREEN = (69, 217, 148)
    CYAN = (69, 217, 202)
    PALE_BLUE = (69, 190, 217)
    DARK_BLUE = (69, 128, 217)
    NIGHT_BLUE = (69, 74, 217)
    PURPLE = (131, 69, 217)
    MAGENTA = (217, 69, 178)
    PINK = (217, 69, 138)


@dataclass(frozen=True)
class Coordinate:
    x: float = 0
    y: float = 0

    def __repr__(self):
        return f'x={self.x}, y={self.y}'


@dataclass
class Direction:
    angle: int = 0

    def __init__(self, angle: int):
        self.angle = (angle + 360) % 360

    def __sub__(self, other):
        return Direction(((self.angle + 360) - other.angle) % 360)


class Reaction(Enum):
    REPULSION = '<->'
    ORIENTATION = '-^-'
    ATTRACTION = '>-<'
    STEADY = '...'
    RIGHT_TURN = '>>>'
    LEFT_TURN = '<<<'


class Edge(Enum):
    LEFT = '|-'
    RIGHT = '-|'
    BOTTOM = '_'
    TOP = '^'
    NONE = '...'


@dataclass(frozen=True)
class Speed:
    meters_per_second: int = 5


@dataclass(frozen=True)
class Horizon:
    meters: int = 10


@dataclass(frozen=True)
class Bird:
    coordinate: Coordinate = Coordinate(x=0, y=0)
    direction: Direction = Direction(angle=0)
    speed: Speed = Speed(meters_per_second=MIN_SPEED)
    horizon: Horizon = Horizon(meters=MIN_HORIZON)
    color: Color = Color.WHITE

    def __repr__(self):
        return f"(" \
               f"c ({self.coordinate.x}, {self.coordinate.y})" \
               f" | b {self.direction.angle}" \
               f" | s {self.speed.meters_per_second}" \
               f" | h {self.horizon.meters}" \
               f")"

    def __eq__(self, other):
        return self.coordinate == other.coordinate \
               and self.direction == other.direction \
               and self.speed == other.speed

    @property
    def x_y(self) -> Tuple[float, float]:
        return (self.coordinate.x, self.coordinate.y)


@dataclass(frozen=True)
class Neighbour:
    boid: Bird
    distance: float
