from math import radians, sin, cos
from typing import List

from boyds.engine.bird import Reaction, Speed, Horizon, Coordinate, Direction, Bird, Neighbour, Color, Edge
from boyds.engine.constants import STEERING_THRESHOLD, CLOSING_THRESHOLD, MAX_HORIZON, HORIZON_INCREMENT, MIN_HORIZON, \
    ACCELERATION_INCREMENT, MIN_SPEED, MAX_SPEED


def chose_reaction(boid: Bird, neighbours: List[Neighbour], edges: List[Edge]) -> Reaction:
    if Edge.LEFT in edges:
        if 180 <= boid.direction.angle <= 270:
            return Reaction.LEFT_TURN
        elif 270 < boid.direction.angle <= 359 or boid.direction.angle == 0:
            return Reaction.RIGHT_TURN
    if Edge.RIGHT in edges:
        if 0 <= boid.direction.angle <= 90:
            return Reaction.LEFT_TURN
        elif 90 < boid.direction.angle <= 180:
            return Reaction.RIGHT_TURN
    if Edge.BOTTOM in edges:
        if 90 <= boid.direction.angle < 180:
            return Reaction.LEFT_TURN
        elif 180 <= boid.direction.angle <= 270:
            return Reaction.RIGHT_TURN
    if Edge.TOP in edges:
        if 270 <= boid.direction.angle <= 359:
            return Reaction.LEFT_TURN
        elif 0 <= boid.direction.angle <= 90:
            return Reaction.RIGHT_TURN

    if any(map(lambda n: n.distance <= STEERING_THRESHOLD, neighbours)):
        return Reaction.REPULSION
    elif any(map(lambda n: n.distance <= CLOSING_THRESHOLD, neighbours)):
        return Reaction.ORIENTATION
    elif any(map(lambda n: n.distance <= boid.horizon.meters, neighbours)):
        return Reaction.ATTRACTION
    else:
        return Reaction.STEADY


def adapt_speed(boid: Bird, reaction: Reaction) -> Speed:
    if reaction == Reaction.ATTRACTION:
        return _accelerate(boid)
    elif reaction == Reaction.REPULSION:
        return _accelerate(boid)
    else:
        return _slow_down(boid)


def adapt_horizon(boid: Bird, reaction: Reaction) -> Horizon:
    if reaction == Reaction.ATTRACTION or reaction == Reaction.STEADY:
        return _extend_horizon(boid)
    else:
        return _restrict_horizon(boid)


def adapt_color(reaction: Reaction) -> Color:
    return {
        Reaction.REPULSION: Color.RED,
        Reaction.ORIENTATION: Color.GREEN,
        Reaction.ATTRACTION: Color.ORANGE,
        Reaction.LEFT_TURN: Color.PINK,
        Reaction.RIGHT_TURN: Color.PINK
    }.get(reaction, Color.WHITE)


def move(boid: Bird, direction: Direction) -> Coordinate:
    mps = boid.speed.meters_per_second
    angle = radians(direction.angle)
    x_prime = mps * sin(angle) + boid.coordinate.x
    y_prime = mps * cos(angle) + boid.coordinate.y
    return Coordinate(round(x_prime, 2), round(y_prime, 2))


def detect_edge(boid: Bird, min_x: int, max_x: int, min_y: int, max_y: int) -> List[Edge]:
    edges = []

    if boid.coordinate.x - min_x <= MAX_HORIZON:
        edges.append(Edge.LEFT)
    if max_x - boid.coordinate.x <= MAX_HORIZON:
        edges.append(Edge.RIGHT)
    if boid.coordinate.y - min_y <= MAX_HORIZON:
        edges.append(Edge.BOTTOM)
    if max_y - boid.coordinate.y <= MAX_HORIZON:
        edges.append(Edge.TOP)

    return edges


def _extend_horizon(boid: Bird) -> Horizon:
    return Horizon(min(MAX_HORIZON, boid.horizon.meters + HORIZON_INCREMENT))


def _restrict_horizon(boid: Bird) -> Horizon:
    return Horizon(max(MIN_HORIZON, boid.horizon.meters - HORIZON_INCREMENT))


def _slow_down(boid: Bird) -> Speed:
    return Speed(max(boid.speed.meters_per_second - ACCELERATION_INCREMENT, MIN_SPEED))


def _accelerate(boid: Bird) -> Speed:
    return Speed(min(boid.speed.meters_per_second + ACCELERATION_INCREMENT, MAX_SPEED))
