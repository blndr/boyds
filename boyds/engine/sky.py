from enum import Enum
from math import radians, atan2, pi, dist
from typing import Union, List

from boyds import Coordinate, Bird
from boyds.engine.bird import Neighbour
from boyds.engine.constants import BLIND_SPOT_AMPLITUDE, MAX_NEIGHBOUR_AWARENESS


class CrowdedSky(Exception):
    def __init__(self, container):
        self.container = container


class Quadrant(Enum):
    NORTH_WEST = 'NW'
    NORTH_EAST = 'NE'
    SOUTH_EAST = 'SE'
    SOUTH_WEST = 'SW'


class Sky:
    def __init__(self, parent, width: int, height: int, quadrant: Union[Quadrant, None] = None):
        self.width: int = width
        self.height: int = height
        self.x_padding = self.width // 2
        self.y_padding = self.height // 2
        self.quadrant: Union[Quadrant, None] = quadrant
        self.parent: Union[Sky, None] = parent
        self.center: Coordinate = self._find_center()
        self.children: List[Sky] = []
        self.boid: Union[Bird, None] = None

    def __repr__(self):
        return f'' \
               f'({self.quadrant.name if not self.is_root else "ROOT"} ' \
               f'| {self.width} * {self.height} ' \
               f'| {self.center} ' \
               f'| {len(self.children)} children ' \
               f'| {1 if self.boid else 0} boid' \
               f')'

    @property
    def is_root(self):
        return self.parent is None

    @property
    def is_leaf(self):
        return not self.children

    @property
    def north_east(self):
        return self._get_child(Quadrant.NORTH_EAST)

    @property
    def north_west(self):
        return self._get_child(Quadrant.NORTH_WEST)

    @property
    def south_east(self):
        return self._get_child(Quadrant.SOUTH_EAST)

    @property
    def south_west(self):
        return self._get_child(Quadrant.SOUTH_WEST)

    @property
    def boids(self):
        boids = []
        if self.boid:
            boids.append(self.boid)
        for child in self.children:
            boids.extend(child.boids)
        return boids

    def contains(self, boid: Bird):
        return not self.is_root and self.boid == boid

    def insert(self, boid: Bird):
        if self.is_root:
            self._insert_into_children(boid)
        else:
            if self.boid:
                if self.boid.coordinate == boid.coordinate:
                    raise CrowdedSky(self)
                else:
                    self._insert_into_children(boid)
            else:
                self.boid = boid

    def find_closest_neighbours(self, boid: Bird) -> List[Neighbour]:
        sub_sky = self._find_sub_sky_containing_boid(boid)
        if not sub_sky:
            return []

        while boid.horizon.meters > sub_sky.x_padding or boid.horizon.meters > sub_sky.y_padding:
            sub_sky = sub_sky.parent

        candidates = filter(lambda b: b != boid, sub_sky.boids)
        other_candidates = map(lambda b: Neighbour(b, distance(boid, b)), candidates)
        close_candidates = filter(lambda n: n.distance < boid.horizon.meters, other_candidates)
        sorted_close_candidates = sorted(close_candidates, key=lambda n: n.distance)

        return sorted_close_candidates[:MAX_NEIGHBOUR_AWARENESS]

    def _find_sub_sky_containing_boid(self, boid: Bird):
        try:
            self.insert(boid)
        except CrowdedSky as e:
            return e.container
        else:
            return None

    def _get_child(self, quadrant: Quadrant):
        if not any(map(lambda c: c.quadrant == quadrant, self.children)):
            return None
        else:
            return next(c for c in self.children if c.quadrant == quadrant)

    def _get_or_create_child(self, quadrant: Quadrant):
        child = self._get_child(quadrant)

        if child is None:
            sub_sky = Sky(self, self.x_padding, self.y_padding, quadrant)
            self.children.append(sub_sky)
            return sub_sky

        return child

    def _insert_into_children(self, boid: Bird):
        quadrant = self._get_quadrant(boid)
        sub_sky = self._get_or_create_child(quadrant)
        sub_sky.insert(boid)

    def _find_center(self):
        if self.is_root:
            return Coordinate(x=self.x_padding, y=self.y_padding)
        else:
            if self.quadrant == Quadrant.NORTH_WEST:
                return Coordinate(
                    x=self.parent.center.x - self.x_padding,
                    y=self.parent.center.y + self.y_padding
                )
            elif self.quadrant == Quadrant.NORTH_EAST:
                return Coordinate(
                    x=self.parent.center.x + self.x_padding,
                    y=self.parent.center.y + self.y_padding
                )
            elif self.quadrant == Quadrant.SOUTH_EAST:
                return Coordinate(
                    x=self.parent.center.x + self.x_padding,
                    y=self.parent.center.y - self.y_padding
                )
            else:
                return Coordinate(
                    x=self.parent.center.x - self.x_padding,
                    y=self.parent.center.y - self.y_padding
                )

    def _get_quadrant(self, boid: Bird) -> Quadrant:
        if boid.coordinate.x < self.center.x and boid.coordinate.y >= self.center.y:
            return Quadrant.NORTH_WEST
        elif boid.coordinate.x >= self.center.x and boid.coordinate.y >= self.center.y:
            return Quadrant.NORTH_EAST
        elif boid.coordinate.x >= self.center.x and boid.coordinate.y < self.center.y:
            return Quadrant.SOUTH_EAST
        else:
            return Quadrant.SOUTH_WEST


def is_in_blind_spot(boid: Bird, neighbour: Bird) -> bool:
    x_prime = neighbour.coordinate.x - boid.coordinate.x
    y_prime = neighbour.coordinate.y - boid.coordinate.y
    boid_polar_angle = radians(-boid.direction.angle) + pi / 2
    neighbour_polar_angle = atan2(y_prime, x_prime)
    neighbour_angle_towards_boid_direction = abs(neighbour_polar_angle - boid_polar_angle) % (2 * pi)
    gap_to_blind_spot = radians((360 - BLIND_SPOT_AMPLITUDE) // 2)
    return neighbour_angle_towards_boid_direction > gap_to_blind_spot


def distance(boid: Bird, neighbour: Bird) -> float:
    result = dist(boid.x_y, neighbour.x_y)
    return round(result, 2)
