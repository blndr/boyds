from boyds import Sky
from boyds.engine.bird import Bird
from boyds.engine.reaction import chose_reaction, adapt_horizon, adapt_speed, move, adapt_color, detect_edge
from boyds.engine.steering import steer


def evolve(boid: Bird, sky: Sky) -> Bird:
    neighbours = sky.find_closest_neighbours(boid)
    edges = detect_edge(boid, 0, sky.width, 0, sky.height)
    reaction = chose_reaction(boid, neighbours, edges)
    direction = steer(boid, reaction, neighbours)
    speed = adapt_speed(boid, reaction)
    horizon = adapt_horizon(boid, reaction)
    color = adapt_color(reaction)
    coordinate = move(boid, direction)
    return Bird(coordinate, direction, speed, horizon, color)
