from typing import List

from boyds import Sky
from boyds.engine.bird import Bird
from boyds.engine.environment import evolve


def interact(flock: List[Bird], sky_width: int, sky_height: int) -> List[Bird]:
    sky = Sky(None, sky_width, sky_height, None)
    for boid in flock:
        sky.insert(boid)

    moved_flock = []
    for boid in flock:
        evolved_boid = evolve(boid, sky)
        moved_flock.append(evolved_boid)

    return moved_flock
