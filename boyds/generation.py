from random import randint
from typing import List

from boyds import Coordinate, Bird, Direction, Speed, Color


def generate(size: int, max_x: int, max_y: int) -> List[Bird]:
    boids = []
    fifth_x, fifth_y = int(max_x / 5), int(max_y / 5)

    for _ in range(size):
        boids.append(Bird(
            Coordinate(x=randint(0, 1 * fifth_x), y=randint(1 * fifth_y, 4 * fifth_y)),
            Direction(angle=randint(85, 95)),
            Speed(meters_per_second=randint(1, 3))
        ))

    return boids


def generate_single(x: int, y: int, angle: int, color: Color):
    return Bird(
        Coordinate(x=x, y=y),
        Direction(angle=angle),
        Speed(meters_per_second=randint(1, 3)),
        color=color
    )
