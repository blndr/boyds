import argparse

import pyglet
from pyglet import clock

from boyds.gui import BoydsWindow

parser = argparse.ArgumentParser(description='Simulate a flock of bird-oids')
parser.add_argument(
    '--size', dest='size', action='store', default=50, help='The size of a flock',
)
parser.add_argument(
    '--duration', dest='duration', action='store', default=None, help='Duration of the simulation in seconds',
)
parser.add_argument(
    '--fps', dest='fps', action='store', default=30, help='Framerate of the simulation',
)
parser.add_argument('--fullscreen', dest='fullscreen', action='store_true')
parser.set_defaults(fullscreen=False)

args = parser.parse_args()

boyds_window = BoydsWindow(args)
clock.schedule_interval(boyds_window.move, 1 / boyds_window.fps)
pyglet.app.run()
